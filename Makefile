########################################
# This file is identical for all notes
# Any changes will get over written
# If you need to change anything, alter
# jrNotes
#######################################
.SUFFIXES: .tex .pdf .Rnw .R .Rmd .html
.PHONY: connect default namer lintr clean cleaner

INCLUDES := $(shell find . -maxdepth 1 -name '*.Rmd' | sort)
NAMES = $(basename $(INCLUDES))

HTML = $(NAMES:=.html)
default: $(HTML)

final: $(HTML)
	Rscript -e "jrPresentation::create_final()"

feedback: 
	virtualenv -p python3 venv
	( \
		. venv/bin/activate; \
		pip install tform; \
		tform create-from-config; \
	)
	Rscript -e 'jrPresentation::create_feedback(); rmarkdown::render("feedback.Rmd")'

# Pushing to connect
connect: $(HTML)
	Rscript -e "jrPresentation::upload_slides()"

# Rename chunks
namer:
	Rscript -e "namer::name_dir_chunks('.')"

.Rmd.html:
	Rscript -e "jrPresentation::build_slide('$<')"

clean:
	Rscript -e "jrNotes::clean()"

cleaner:
	make clean
	Rscript -e "jrNotes::cleaner()"
	Rscript -e "jrPresentation::clean_site()"

